var express = require('express')
var cors = require('cors')
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(cors());

let todos = [
    {id:1, title:'Create a todo!', completed:false}
];

app.get('/api/todos', (req, res)=> res.json(todos));
app.post('/api/todos', (req, res) =>{
    var todo = req.body;
    todo.id = todos.length+1;
    todos.push(todo);
    res.send(todo);
});

app.patch('/api/todos/:id/complete', (req, res)=>{
    let id = req.params.id;
    let todo = todos.find((x)=> x.id == id);
    todo.completed = !todo.completed;
    res.send(todo);
});

app.delete('/api/todos/:id', (req, res)=>{
    let id = req.params.id;
    todos = todos.filter((x) => x.id != id);
    res.sendStatus(200);
});

app.listen(4000, (err)=>{
    console.log('listening on 4000');
})